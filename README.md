# VDeck_api

## 概要

## 環境構築

## 各種コマンド

### Docker

```bash
$ docker-compose start
// run mySQL server
```

### Docs

Docs を生成する

```bash
$ yardoc "app/**/*"
```

Docs サーバを立ち上げる

```bash
$ yard server --reload
```

---

## コーディング規則

### CI

- [GitLab CI で知っておくと便利かもしれない tips - Qiita](https://qiita.com/kytiken/items/a95ef8c1fccfc4a9b089)

### テスト

#### Arrange/Act/Assert(AAA) の意識

> - Arrange（準備）= let/let!、もしくは before の中（場合によっては it の中でも可）
> - Act（実行）= it の中
> - Assert（検証）= it の中

#### クエリ

<https://qiita.com/yosaprog/items/dbfc9f5194b49b5222c6>
