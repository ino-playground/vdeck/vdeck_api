# frozen_string_literal: true

FactoryBot.define do
  factory :channel do
    channel_id { "UC#{Faker::Alphanumeric.alphanumeric(number: 22)}" }
    title { "#{Faker::Name.first_name} channel" }
  end
end
