# frozen_string_literal: true

FactoryBot.define do
  factory :video do
    channel
    video_id { Faker::Alphanumeric.alphanumeric(number: 10) }
    title { 'dummy video title' }
  end
end
