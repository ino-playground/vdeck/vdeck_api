# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Channels', type: :request do
  describe 'GET /channels' do
    let(:channel_num) { 3 }
    let!(:channels) { create_list(:channel, channel_num) }

    it 'returns an index of channels' do
      get channels_path, params: { page: 1, per: 25 }

      resp = default_validation_and_return_response_body
      expect(resp.size).to eq channel_num
      expect(resp.pluck('id')).to match channels.pluck(:id)
    end
  end

  describe 'POST /channels' do
    let(:channel_request) do
      {
        channel_id: 'ABCDEFGHIJ01234',
        title: 'Hoge channel',
        description: 'fugafuga'
      }
    end
    let(:invalid_channel_request) do
      {
        channel_id: Channel.first.channel_id,
        title: 'Hoge channel',
        description: 'fugafuga'
      }
    end
    let(:channel_num) { 3 }

    before do
      create_list(:channel, channel_num)
    end

    it 'returns a created channels' do
      post channels_path, params: { channel: channel_request }

      resp = default_validation_and_return_response_body
      expect(resp['channel_id']).to eq channel_request[:channel_id]
      expect(Channel.count).to eq channel_num.succ
    end

    it 'returns 400 if channel_id is conflicted' do
      post channels_path, params: { channel: invalid_channel_request }

      expect(response).to have_http_status(:bad_request)
      assert_response_schema_confirm
    end
  end

  describe 'GET /channels/{id}' do
    let(:channel) { create(:channel) }

    before do
      create_list(:channel, 3)
    end

    it 'returns a designated channel' do
      get channel_path(channel.id)

      resp = default_validation_and_return_response_body
      expect(resp['channel_id']).to eq channel.channel_id
    end

    it 'returns 404 if not found' do
      get channel_path(1000)

      expect(response).to have_http_status(:not_found)
      assert_response_schema_confirm
    end
  end

  describe 'GET /channels/{id}/videos' do
    let(:video_num) { 3 }
    let(:channel) { create(:channel) }
    let!(:videos) { create_list(:video, video_num, channel: channel) }

    before do
      create_list(:video, 5)
    end

    it 'returns videos of designated channel' do
      get videos_channel_path(channel.id)

      resp = default_validation_and_return_response_body
      expect(resp.size).to be video_num
      expect(resp.pluck('channel_id')).to all be channel.id
      expect(resp.pluck('video_id')).to match videos.pluck(:video_id)
    end

    it 'returns 404 if not found' do
      get videos_channel_path(1000)

      expect(response).to have_http_status(:not_found)
      assert_response_schema_confirm
    end
  end

  describe 'PUT /channels/{id}' do
    let(:channel) { create(:channel) }
    let(:channel_request) do
      {
        channel_id: channel.channel_id,
        title: 'Hoge channel',
        description: 'fugafuga'
      }
    end
    let(:invalid_channel_request) do
      {
        channel_id: 'invalid_channel_id',
        title: 'Hoge channel',
        description: 'fugafuga'
      }
    end

    before do
      create_list(:channel, 3)
    end

    it 'returns a designated channel' do
      put channel_path(channel.id), params: { channel: channel_request }

      _ = default_validation_and_return_response_body
      expect { channel.reload }.to avoid_changing(channel, :channel_id).and change(channel, :title).to(channel_request[:title])
    end

    it 'returns 400 if try to update channel_id' do
      put channel_path(channel.id), params: { channel: invalid_channel_request }

      expect(response).to have_http_status(:bad_request)
      expect { channel.reload }.to avoid_changing(channel, :channel_id)
    end

    it 'returns 404 if not found' do
      put channel_path(1000), params: { channel: channel_request }

      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'DELETE /channels/{id}' do
    let(:channel) { create(:channel) }

    before do
      create_list(:channel, 3)
    end

    it 'returns a designated channel' do
      delete channel_path(channel.id)

      _ = default_validation_and_return_response_body
      expect(Channel.where(id: channel.id)).not_to exist
    end

    it 'returns 404 if not found' do
      delete channel_path(1000)

      expect(response).to have_http_status(:not_found)
      expect(Channel.where(id: channel.id)).to exist
    end
  end
end
