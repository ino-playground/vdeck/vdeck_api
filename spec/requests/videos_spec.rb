# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Videos', type: :request do
  describe 'GET /videos' do
    let(:video_num) { 3 }
    let!(:videos) { create_list(:video, video_num) }

    it 'returns an index of videos' do
      get videos_path, params: { page: 1, per: 25 }

      resp = default_validation_and_return_response_body
      expect(resp.size).to eq video_num
      expect(resp.pluck('id')).to match videos.pluck(:id)
    end
  end

  describe 'POST /videos' do
    let(:channel) { create(:channel) }
    let(:video_request) do
      {
        video_id: 'ABCDEFGHIJ01234',
        channel_id: channel.id,
        title: 'Hoge video',
        description: 'fugafuga'
      }
    end
    let(:video_num) { 3 }

    before do
      create_list(:video, video_num)
    end

    it 'returns a created videos' do
      post videos_path, params: { video: video_request }

      resp = default_validation_and_return_response_body
      expect(resp['video_id']).to eq video_request[:video_id]
      expect(Video.count).to eq video_num.succ
      assert_response_schema_confirm
    end
  end

  describe 'GET /videos/{id}' do
    let(:channel) { create(:channel) }
    let(:video) { create(:video, channel: channel) }

    before do
      create_list(:video, 3)
    end

    it 'returns a designated video' do
      get video_path(video.id)

      resp = default_validation_and_return_response_body
      expect(resp['video_id']).to eq video.video_id
    end

    it 'returns 404 if not found' do
      get video_path(1000)

      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'PUT /videos/{id}' do
    let(:channel) { create(:channel) }
    let(:video) { create(:video, channel: channel) }
    let(:video_request) do
      {
        video_id: video.video_id,
        channel_id: video.channel_id,
        title: 'Hoge video',
        description: 'fugafuga'
      }
    end
    let(:invalid_video_request) do
      {
        video_id: 'HOGEHOGE12345',
        channel_id: video.channel_id,
        title: 'Hoge video',
        description: 'fugafuga'
      }
    end

    before do
      create_list(:video, 3)
    end

    it 'returns a designated video' do
      put video_path(video.id), params: { video: video_request }

      _ = default_validation_and_return_response_body
      expect { video.reload }.to avoid_changing(video, :video_id).and change(video, :title).to(video_request[:title])
    end

    it 'returns 400 if try to update video_id' do
      put video_path(video.id), params: { video: invalid_video_request }

      expect(response).to have_http_status(:bad_request)
      expect { video.reload }.to avoid_changing(video, :video_id)
    end

    it 'returns 404 if not found' do
      put video_path(1000), params: { video: video_request }

      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'DELETE /videos/{id}' do
    let(:channel) { create(:channel) }
    let(:video) { create(:video, channel: channel) }

    before do
      create_list(:video, 3)
    end

    it 'returns a designated video' do
      delete video_path(video.id)

      _ = default_validation_and_return_response_body
      expect(Video.where(id: video.id)).not_to exist
    end

    it 'returns 404 if not found' do
      delete video_path(1000)

      expect(response).to have_http_status(:not_found)
      expect(Video.where(id: video.id)).to exist
    end
  end
end
