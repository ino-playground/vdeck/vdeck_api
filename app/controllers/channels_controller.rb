# frozen_string_literal: true

# Controller: _"ChannelsController"_ を定義
class ChannelsController < ApplicationController
  before_action :find_channel_by_id, except: %i[index create]

  # return All channels
  def index
    channels = Channel.all
    render json: channels.page(params[:page]).per(params[:per])
  end

  # return channel of id
  def show
    render json: @channel
  end

  # return videos which belong to channel of _id
  def videos
    render json: @channel.videos.page(params[:page]).per(params[:per])
  end

  # create new channel
  def create
    channel = Channel.create!(channel_params).reload
    render json: channel
  end

  # update channel data
  def update
    raise ActiveRecord::RecordInvalid unless update_valid?(channel_params)

    @channel.update!(channel_params)
    render json: @channel
  end

  # destroy a channel
  def destroy
    @channel.destroy!
    render json: { status: 200, message: 'Successfully deleted.' }
  end

  private

  # find channel by id
  #
  # @return [Channel] a Channel record if find
  def find_channel_by_id
    @channel = Channel.unscoped.find(params[:id])
  end

  # strong parameter of channel
  def channel_params
    params.require(:channel).permit(
      :channel_id,
      :title,
      :description
    )
  end

  # validation for update
  def update_valid?(params)
    immutable_columns = Channel.immutable_attributes
    @channel.slice(immutable_columns).transform_values(&:to_s) == params.slice(*immutable_columns).to_h
  end
end
