# frozen_string_literal: true

class ApplicationController < ActionController::API
  unless Rails.env.development?
    rescue_from Exception, with: :render500
    rescue_from ActiveRecord::RecordNotFound, with: :render404
    rescue_from ActionController::BadRequest, with: :render400
    rescue_from ActiveRecord::RecordInvalid,      with: :render400
    rescue_from ActionController::RoutingError,   with: :render404
    rescue_from ActionController::ParameterMissing, with: :render400
    rescue_from NotImplementedError, with: :render501
    rescue_from NoMemoryError, ScriptError, Interrupt, SecurityError, SignalException, SystemExit, SystemStackError, with: :only_logging
  end

  def routing_error
    raise ActionController::RoutingError, params[:path]
  end

  private

  def render400(err = nil)
    render json: { status: 400, message: err.message }, status: :bad_request
    nil
  end

  def render401(err = nil)
    render json: { status: 401, message: err.message }, status: :unauthorized
    nil
  end

  def render403(err = nil)
    render json: { status: 403, message: err.message }, status: :forbidden
    nil
  end

  def render404(err = nil)
    render json: { status: 404, message: err.message }, status: :not_found
    nil
  end

  def render500(err = nil)
    render json: { status: 500, message: err.message }, status: :internal_server_error
    nil
  end

  def render501(err = nil)
    render json: { status: 501, message: err.message }, status: :not_implemented
    nil
  end

  def render_503_in_maintenance
    render json: { status: 503, message: Settings.env.maintenance_sentence }, status: :service_unavailable
    nil
  end
end
