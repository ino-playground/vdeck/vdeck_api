# frozen_string_literal: true

# Controller: _"VideosController"_ を定義
class VideosController < ApplicationController
  before_action :find_video_by_id, except: %i[index create]

  # return all videos
  def index
    render json: Video.includes(:channel).page(params[:page]).per(params[:per])
  end

  # return video of id
  def show
    render json: @video
  end

  # create new video
  def create
    new_video = Video.create!(video_params).reload
    render json: new_video
  end

  # update a video
  def update
    raise ActiveRecord::RecordInvalid unless update_valid?(video_params)

    @video.update!(video_params)
    render json: @video
  end

  # destroy a video
  def destroy
    @video.destroy!
    render json: { status: 200, message: 'Successfully deleted.' }
  end

  private

  # find a video by id
  #
  # @return [Video] a Channel record if find
  def find_video_by_id
    @video = Video.unscoped.find(params[:id])
  end

  # strong parameter of video
  def video_params
    params.require(:video).permit(
      :video_id,
      :channel_id,
      :title,
      :description
    )
  end

  # validation for update
  def update_valid?(params)
    immutable_columns = Video.immutable_attributes
    @video.slice(immutable_columns).transform_values(&:to_s) == params.slice(*immutable_columns).to_h
  end
end
