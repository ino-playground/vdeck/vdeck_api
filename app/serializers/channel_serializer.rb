# frozen_string_literal: true

class ChannelSerializer < ActiveModel::Serializer
  attributes(*Channel.column_names)
end
