# frozen_string_literal: true

class VideoSerializer < ActiveModel::Serializer
  belongs_to :channel

  attributes(*Video.column_names)
end
