# frozen_string_literal: true

class Video < ApplicationRecord
  validates :video_id, presence: true, uniqueness: { case_sensitive: true }
  validates :title, presence: true

  belongs_to :channel

  attr_reader :immutable_attributes

  def self.immutable_attributes
    column_names && %w[video_id channel_id]
  end
end
