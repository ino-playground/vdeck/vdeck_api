# frozen_string_literal: true

class Channel < ApplicationRecord
  validates :channel_id, presence: true, uniqueness: { case_sensitive: true }
  validates :title, presence: true

  has_many :videos, dependent: :destroy

  attr_reader :immutable_attributes

  def self.immutable_attributes
    column_names && %w[channel_id]
  end
end
