# frozen_string_literal: true

# Get YouTube Videos Info
module Youtube
  require 'google/apis/youtube_v3'
  require 'json'

  require 'nokogiri'
  require 'active_support/core_ext'
  require 'open-uri'

  # Get archives via YouTube Data API
  class RequestArchive
    def fetch_activities_list(channel_id,
                              since_date = Time.zone.now.ago(1.hour).iso8601)
      activities_response = youtube_service.list_activities('snippet, contentDetails',
                                                            channel_id: channel_id,
                                                            published_after: since_date)
      activities_response.items.map do |activity|
        if activity.snippet.type != 'upload' \
          || activity.snippet.channel_id != channel_id
          next
        end

        {
          video_id: activity.content_details.upload.video_id,
          title: activity.snippet.title,
          published_at: activity.snippet.published_at,
          channel_id: activity.snippet.channel_id,
          description: activity.snippet.description
          # video_thumbnail: activity.snippet.thumbnails.medium.url,
        }
      end
    end

    private

    def youtube_service
      service = Google::Apis::YoutubeV3::YouTubeService.new
      service.key = Settings.youtube.api_key
      service
    end
  end

  # Get archives via RSS
  class RequestRSS
    def fetch_from_rss(channel_id,
                       since_date = Time.zone.now.ago(25.hours),
                       until_date = Time.zone.now.ago(1.hour))
      xml_url = "https://www.youtube.com/feeds/videos.xml?channel_id=#{channel_id}"
      hash = Array((Hash.from_xml URI.parse(xml_url).open.read)['feed']['entry'])
      hash.map do |video|
        next unless Time.zone.parse(video['published']) >= since_date \
          && Time.zone.parse(video['published']) <= until_date \
          && video['group']['community']['statistics']['views'] != '0'

        {
          video_id: video['videoId'],
          title: video['title'],
          published_at: Time.zone.parse(video['published']),
          channel_id: video['channelId'],
          description: video['group']['description']
        }
      end.compact
    end
  end

  # Get Live stream via YouTube Data API
  class RequestLive
    def fetch_streaming_info(channel_ids)
      live_ids = get_stream_ids(channel_ids)
      return nil if live_ids.blank?

      videos_response = youtube_service.list_videos('snippet, liveStreamingDetails', id: live_ids)
      videos_response.items.each_with_object({}) do |response_detail, result|
        stream_type, info = live_status_filter(response_detail)
        (result[stream_type] ||= []) << info
      end
    end

    def get_stream_ids(channel_ids)
      channel_ids.map do |channel_id|
        embedded_url = "https://www.youtube.com/embed/live_stream?channel=#{channel_id}"
        doc = Nokogiri::HTML(URI.parse(embedded_url).open)
        doc.xpath('//a').map do |el|
          extract_url = el[:href]
          live_id = extract_url[(extract_url.index('=') + 1)..]
          live_id if live_id.size == 11
        end
        # Presumably live_ids.size <= 1
        # (In other words, "each" statement may be only executed once, at most)
        # But the following may occer.
        #   * the number of elements could be 2 or more
        #   * it could not return the correct video_ID (11 digits)
        # I'm making a instant exception handling here,
        # but you should note that it could be a bug factor.
      end.flatten
    end

    private

    def live_status_filter(response_detail)
      threshold_date = Time.zone.now.since(1.month).iso8601
      if response_detail.snippet.live_broadcast_content == 'upcoming' \
        && response_detail.live_streaming_details.scheduled_start_time <= Time.zone.parse(threshold_date) \
        || response_detail.snippet.live_broadcast_content == 'live'
        info = {
          video_id: response_detail.id,
          title: response_detail.snippet.title,
          start_at: response_detail.live_streaming_details.scheduled_start_time,
          live_status: response_detail.snippet.live_broadcast_content,
          channel_id: response_detail.snippet.channel_id,
          description: response_detail.snippet.description
        }
        [response_detail.snippet.live_broadcast_content.intern, info]
      end
    end

    def youtube_service
      service = Google::Apis::YoutubeV3::YouTubeService.new
      service.key = Settings.youtube.api_key
      service
    end
  end
end
