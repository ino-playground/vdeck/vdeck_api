source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.2'

gem 'active_model_serializers'
gem 'bootsnap', '>= 1.4.4', require: false # Reduces boot times through caching; required in config/boot.rb
gem 'committee'
gem 'committee-rails'
gem 'config'
gem 'mysql2', '~> 0.5' # Use mysql as the database for Active Record
gem 'puma', '~> 5.0' # Use Puma as the app server
gem 'rack-cors' # Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rails', '~> 6.1.3', '>= 6.1.3.2' # Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'rails-i18n'

# Google API
gem 'google-api-client', '~> 0.53', require: 'google/apis/youtube_v3'

# Utils
gem 'kaminari'
gem 'nokogiri'

# Sidekiq
gem 'sidekiq'
gem 'sidekiq-scheduler'

# Logging
gem 'bullet'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'
# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rails_best_practices', require: false
  gem 'rspec-rails'

  # Pry
  gem 'better_errors'
  gem 'pry', '~> 0.13.0'
  gem 'pry-byebug'
  gem 'pry-doc'
  gem 'pry-rails'

  # Create dummy data
  gem 'factory_bot_rails'
  gem 'faker'
end

group :development do
  gem 'listen', '~> 3.3'
  gem 'rubocop', require: false
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  gem 'spring' # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring-watcher-listen'

  gem 'brakeman', require: false
  gem 'rails-erd'
end

group :test do
  gem 'simplecov'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
