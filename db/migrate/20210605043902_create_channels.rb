# frozen_string_literal: true

class CreateChannels < ActiveRecord::Migration[6.1]
  def change
    create_table :channels do |t|
      t.string :channel_id, null: false
      t.string :title, null: false
      t.text :description

      t.datetime :created_at, default: -> { 'NOW()' }, null: false
      t.datetime :updated_at, default: -> { 'NOW()' }, null: false
    end

    add_index :channels, :channel_id, unique: true
  end
end
