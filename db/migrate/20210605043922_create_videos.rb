# frozen_string_literal: true

class CreateVideos < ActiveRecord::Migration[6.1]
  def change
    create_table :videos do |t|
      t.string :video_id, null: false, unique: true
      t.string :title, null: false
      t.references :channel, foreign_key: true, null: false
      t.datetime :published_at
      t.text :description

      t.datetime :created_at, default: -> { 'NOW()' }, null: false
      t.datetime :updated_at, default: -> { 'NOW()' }, null: false
    end

    add_index :videos, :video_id, unique: true
  end
end
