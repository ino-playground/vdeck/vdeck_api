# frozen_string_literal: true

channel_data = ActiveSupport::JSON.decode(File.read('./db/lib/channels_small.json'))
channel_data['items'].each do |data|
  Channel.create(
    channel_id: data['channelId'],
    title: data['title']
  )
end

Channel.all.map do |channel|
  rss_request = Youtube::RequestRSS.new
  rss_list = rss_request.fetch_from_rss(channel.channel_id, Time.zone.now.ago(7.days))
  rss_list.each do |video|
    Video.create(
      video_id: video[:video_id],
      title: video[:title],
      published_at: video[:published_at],
      channel_id: channel.id,
      description: video[:description]
    )
    # TODO: Rewrite the following appropriately
    # thumbnail: video['videoImgs']['medium']['url']
  end
end

# User.create({
#               username: '御城みつき',
#               email: 'oshiro@sample.com',
#               password: 'testtest123',
#               password_confirmation: 'testtest123'
#             })
# User.create({
#               username: '風利えれな',
#               email: 'elena@example.co.jp',
#               password: 'fizzbazz15',
#               password_confirmation: 'fizzbazz15'
#             })
# ChannelUser.create({
#                      user_id: User.first.id,
#                      channel_id: Channel.first.id
#                    })
# ChannelUser.create({
#                      user_id: User.first.id,
#                      channel_id: Channel.last.id
#                    })
