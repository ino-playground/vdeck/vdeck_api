Rails.application.routes.draw do
  resources :videos, except: %i[]
  resources :channels, except: %i[] do
    member do
      get :videos
    end
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
